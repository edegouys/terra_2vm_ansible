resource "azurerm_resource_group" "rg" {
    name= "${var.name_rg}"
    location = "${var.location}"

    tags {
        owner= "${var.owner}"
    }
}
resource "azurerm_virtual_network" "vNet" {
    name= "${var.name_vnet}"
    address_space= ["${var.address_space}"]
    location= "${azurerm_resource_group.rg.location}"
    resource_group_name= "${azurerm_resource_group.rg.name}"  
}

resource "azurerm_subnet" "subNet1" {
    name= "${var.name_subNet}_1"
    resource_group_name= "${azurerm_resource_group.rg.name}" 
    virtual_network_name= "${azurerm_virtual_network.vNet.name}"
    address_prefix= "10.0.1.0/24"
}

resource "azurerm_subnet" "subNet2" {
    name= "${var.name_subNet}_2"
    resource_group_name= "${azurerm_resource_group.rg.name}" 
    virtual_network_name= "${azurerm_virtual_network.vNet.name}"
    address_prefix= "10.0.2.0/24"
}

resource "azurerm_network_security_group" "NSecure1" {
  name= "${var.name_secure}_1"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  

    security_rule {
        name= "SSH"
        priority= "1001"
    #incrémenter à chaque création de port"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "22"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }
    security_rule {
        name= "HTTP"
        priority= "1002"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "80"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }

    security_rule {
        name= "jenkins"
        priority= "1003"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "8080"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }  
}

resource "azurerm_network_security_group" "NSecure2" {
  name= "${var.name_secure}_2"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  

    security_rule {
        name= "SSH"
        priority= "1004"
    #incrémenter à chaque création de port"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "22"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }
    security_rule {
        name= "HTTP"
        priority= "1005"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "80"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }
}

resource "azurerm_public_ip" "IP1" {
  name= "${var.name_IP}_1"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  allocation_method= "Static"
}

resource "azurerm_public_ip" "IP2" {
  name= "${var.name_IP}_2"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  allocation_method= "Static"
}

resource "azurerm_network_interface" "NIC1" {
  name= "${var.name_ni}_1"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}" 
  network_security_group_id= "${azurerm_network_security_group.NSecure1.id}"

  ip_configuration {
    name= "${var.ip_config}_1"
    subnet_id= "${azurerm_subnet.subNet1.id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id= "${azurerm_public_ip.IP1.id}"
  }
}

resource "azurerm_network_interface" "NIC2" {
  name= "${var.name_ni}_2"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}" 
  network_security_group_id= "${azurerm_network_security_group.NSecure2.id}"

  ip_configuration {
    name= "${var.ip_config}_2"
    subnet_id= "${azurerm_subnet.subNet2.id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id= "${azurerm_public_ip.IP2.id}"
  }
}

resource "azurerm_managed_disk" "ManDisk1" {
 name= "disk_1"
 location= "${azurerm_resource_group.rg.location}"
 resource_group_name= "${azurerm_resource_group.rg.name}"
 storage_account_type= "Standard_LRS"
 create_option= "Empty"
 disk_size_gb= "2048"
}

resource "azurerm_managed_disk" "ManDisk2" {
 name= "disk_2"
 location= "${azurerm_resource_group.rg.location}"
 resource_group_name= "${azurerm_resource_group.rg.name}"
 storage_account_type= "Standard_LRS"
 create_option= "Empty"
 disk_size_gb= "2048"
}

resource "azurerm_virtual_machine" "VM1" {
  name= "${var.name_vm}_1"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  network_interface_ids= [ "${azurerm_network_interface.NIC1.id}" ]
  vm_size= "${var.vmSize}"

  storage_image_reference {
    publisher= "OpenLogic"
    offer= "CentOS"
    sku= "7.5"
    version= "latest"
  }

  storage_os_disk {
    name= "osdisk1"
    caching= "ReadWrite"
    create_option= "FromImage"
    managed_disk_type= "Standard_LRS"
  }

  storage_data_disk {
   name= "${azurerm_managed_disk.ManDisk1.name}"
   managed_disk_id = "${azurerm_managed_disk.ManDisk1.id}"
   create_option   = "Attach"
   lun             = 1
   disk_size_gb    = "${azurerm_managed_disk.ManDisk1.disk_size_gb}"
  }

  os_profile {
    computer_name= "pepette"
    admin_username= "estelle"
  }

  os_profile_linux_config {
      disable_password_authentication= true

      ssh_keys {
          path= "/home/estelle/.ssh/authorized_keys"
          key_data= "${var.key_data}"
      }
  }
}

resource "azurerm_virtual_machine" "VM2" {
  name= "${var.name_vm}_2"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  network_interface_ids= [ "${azurerm_network_interface.NIC2.id}" ]
  vm_size= "${var.vmSize}"

  storage_image_reference {
    publisher= "OpenLogic"
    offer= "CentOS"
    sku= "7.5"
    version= "latest"
  }

  storage_os_disk {
    name= "osdisk2"
    caching= "ReadWrite"
    create_option= "FromImage"
    managed_disk_type= "Standard_LRS"
  }

  storage_data_disk {
   name= "${azurerm_managed_disk.ManDisk2.name}"
   managed_disk_id = "${azurerm_managed_disk.ManDisk2.id}"
   create_option   = "Attach"
   lun             = 1
   disk_size_gb    = "${azurerm_managed_disk.ManDisk2.disk_size_gb}"
  }

  os_profile {
    computer_name= "pepette"
    admin_username= "estelle"
  }

  os_profile_linux_config {
      disable_password_authentication= true

      ssh_keys {
          path= "/home/estelle/.ssh/authorized_keys"
          key_data= "${var.key_data}"
      }
  }
}